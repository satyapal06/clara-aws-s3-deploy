from bitbucket_pipes_toolkit import Pipe
import boto3
import os
variables = {
  'ACCESS_KEY': {'type': 'string', 'required': True},
  'SECRET_KEY': {'type': 'string', 'required': True},
  'REGION_NAME': {'type': 'string', 'required': True},
  'OPERATION': {'type': 'string', 'required': False, 'default': 'S3-TO-S3'},
  'SOURCE_BUCKET_NAME': {'type': 'string', 'required': False, 'default': ''},
  'SOURCE_PATH': {'type': 'string', 'required': False, 'default': ''},
  'DESTINATION_BUCKET_NAME': {'type': 'string', 'required': False, 'default': ''},
  'DESTINATION_PATH': {'type': 'string', 'required': False, 'default': ''},
  'LOCAL_PATH': {'type': 'string', 'required': False, 'default': ''},
  'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}
pipe = Pipe(schema=variables)
pipe.log_info("Executing the pipe...")
ACCESS_KEY = pipe.get_variable('ACCESS_KEY')
SECRET_KEY = pipe.get_variable('SECRET_KEY')
REGION_NAME = pipe.get_variable('REGION_NAME')
OPERATION = pipe.get_variable('OPERATION')
SOURCE_BUCKET_NAME = pipe.get_variable('SOURCE_BUCKET_NAME')
SOURCE_PATH = pipe.get_variable('SOURCE_PATH')
DESTINATION_BUCKET_NAME = pipe.get_variable('DESTINATION_BUCKET_NAME')
DESTINATION_PATH = pipe.get_variable('DESTINATION_PATH')
LOCAL_PATH = pipe.get_variable('LOCAL_PATH')
DEBUG = pipe.get_variable('DEBUG')
pipe.log_info(f"ACCESS_KEY, {ACCESS_KEY}")
pipe.log_info(f"SECRET_KEY, {SECRET_KEY}")
pipe.log_info(f"REGION_NAME, {REGION_NAME}")
pipe.log_info(f"OPERATION, {OPERATION}")
pipe.log_info(f"SOURCE_BUCKET_NAME, {SOURCE_BUCKET_NAME}")
pipe.log_info(f"SOURCE_PATH, {SOURCE_PATH}")
pipe.log_info(f"DESTINATION_BUCKET_NAME, {DESTINATION_BUCKET_NAME}")
pipe.log_info(f"DESTINATION_PATH, {DESTINATION_PATH}")
pipe.log_info(f"LOCAL_PATH, {LOCAL_PATH}")
pipe.log_info(f"DEBUG, {DEBUG}")
s3 = boto3.client('s3', region_name=REGION_NAME, aws_access_key_id=ACCESS_KEY, 
    aws_secret_access_key=SECRET_KEY)
s3_resource = boto3.resource('s3', region_name=REGION_NAME, aws_access_key_id=ACCESS_KEY,
    aws_secret_access_key=SECRET_KEY)
if OPERATION == 'S3-TO-S3':
    keys = s3.list_objects(Bucket=SOURCE_BUCKET_NAME, Prefix=SOURCE_PATH)
    for key in keys['Contents']:
        pipe.log_info(f"Key, {key['Key']}")
        head_tail = os.path.split(key['Key'])
        file_name = head_tail[1]
        pipe.log_info(f"File Name:, {file_name}")
        copy_source = { 'Bucket': SOURCE_BUCKET_NAME, 'Key': key['Key'] }
        pipe.log_info(f"Copy Source:, {copy_source}")
        destination_key = DESTINATION_PATH + '/' + file_name
        pipe.log_info(f"Destination Key:, {destination_key}")
        s3.copy(copy_source, DESTINATION_BUCKET_NAME, destination_key)
else :
    keys = s3.list_objects(Bucket=SOURCE_BUCKET_NAME, Prefix=SOURCE_PATH)
    for key in keys['Contents']:
        pipe.log_info(f"Key, {key['Key']}")
        head_tail = os.path.split(key['Key'])
        file_name = head_tail[1]
        pipe.log_info(f"File Name:, {file_name}")
        s3_resource.Bucket(SOURCE_BUCKET_NAME).download_file(key['Key'], LOCAL_PATH + '/' + file_name)
pipe.success(message="Success!")