FROM python:3.7-slim 

COPY pipe.py /
RUN pip --version
RUN pip install bitbucket_pipes_toolkit 
RUN pip install boto3

ENTRYPOINT ["python3", "/pipe.py"]